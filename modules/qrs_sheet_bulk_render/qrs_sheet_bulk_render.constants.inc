<?php

/**
 * @file
 * Constants used by the "QR-Code Sticker Sheet Bulk Renderer" module.
 *
 * @author
 * Guy Paddock (gapopl@rit.edu)
 */

/**
 * The name of the key in the 'sandbox' of the Batch API context array that contains the current sheet index.
 *
 * @var string
 */
define('QRSBR_SHEET_INDEX_KEY', 'sheetIndex');

/**
 * The maximum number of QR-code sticker sheets to render to PDF during a single batch request. The higher this
 * number, the faster the batch process will progress, but the greater the chance Drupal will exceed the PHP
 * script time limit.
 *
 * @var integer
 */
define('QRSBR_MAX_SHEETS_PER_REQUEST', 75);

/**
 * The menu path for the page that explains the QR-code sticker sheet rendering process.
 *
 * @var string
 */
define('QRSBR_PATH_PAGE', 'admin/content/render-all-qrs-sheets');


/**
 * The menu path that actually triggers the PDF rendering process.
 *
 * @var string
 */
define('QRSBR_PATH_PROCESS', QRSBR_PATH_RENDER_ALL . '/process');

/**
 * The name of the theme callback for displaying the page for rendering all QR-code sticker sheets to a single
 * PDF.
 *
 * @var string
 */
define('QRSBR_THEME_RENDER_PAGE', 'qrs_sheet_bulk_render_page');

//  All permissions
/**
 * Name of the permission for being able to view all QR-code PDF sticker sheets in one giant, bulk PDF.
 *
 * @var string
 */
define('QRSBR_PERM_VIEW_ALL', 'view all QR-code PDF sticker sheet content (warning: resource intensive)');
