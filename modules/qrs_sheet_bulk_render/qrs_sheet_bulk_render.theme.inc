<?php

/**
 * @file
 * Theme functions for the bulk rendering module.
 *
 * @author
 * Guy Paddock (gapopl@rit.edu)
 */

/**
 * A theme function for returning the page from which the user can initiate the process of rendering all sticker sheets
 * to a single PDF file. This page provides the user with the opportunity to learn about the possibly time-consuming
 * bulk QR-code sticker sheet PDF rendering process, and to bail out, if necessary, before he or she begins it.
 *
 * @return
 *    A themed HTML string containing the content of the initial "Render all QR-code sticker sheets" page.
 */
function theme_qrs_sheet_bulk_render_page() {
  $output =
    '<p>' . t('From this page, you can render all QR-code sticker sheet nodes into a single Adobe Acrobat PDF file, '.
    'which is often more convenient when you wish to print all pre-generated sticker sheets in a single print job. ') .
    '</p>';

  $output .=
    '<p>' . t('Please note that, depending on how many QR-code sticker sheet nodes exist on this site, this process '.
    'may take anywhere from a few seconds to several minutes to complete and cannot be easily canceled. Also, note '.
    'that <strong>the final PDF document is not saved on the server</strong>; please save it locally after '.
    'the rendering process is complete.') . '</p>';

  $output .=
    '<p>' . t('To start the process, click the &quot;Start rendering process&quot; button below.') . '</p>';

  /* Load a small form so we can get use a submit button instead of a link to initiate the action.
   *
   * Some may argue that this should be a link, but since the user will be creating a batch process by invoking the
   * callback URL, they are changing some state server-side, so it makes more intuitive sense to make this a
   * button.
   */
  $output .= drupal_get_form('qrs_sheet_bulk_render_form');

  return $output;
}

/**
 * A small form that just has the "start rendering" button.
 *
 * @param $form_state
 *    A keyed array containing the form state. This parameter is not used.
 *
 * @return
 *    An array representing the components of the "render all" form.
 *
 */
function qrs_sheet_bulk_render_form(&$form_state) {
  $form   = array(
    // Send the user on to the callback page to start the batch process
    '#redirect' => QRSBR_PATH_PROCESS,

    'start'     => array(
      '#type'   => 'submit',
      '#value'  => t('Start rendering process')
    )
  );

  return $form;
}
