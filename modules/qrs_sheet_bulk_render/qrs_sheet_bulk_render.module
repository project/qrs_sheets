<?php

// The internal name for this module
define('QRSBR_MODULE', 'qrs_sheet_bulk_render');

module_load_include('inc', QRSBR_MODULE, QRSBR_MODULE . '.constants');

/**
 * Implementation of hook_menu().
 *
 * @return
 *    An array of the menu items for this module.
 */
function qrs_sheet_bulk_render_menu() {
  $items = array(
    QRSBR_PATH_PAGE => array(
      'title'             => 'Render all QR-code sticker sheets to PDF',
      'description'       => 'Renders all QR-code sticker sheet nodes into a single, easy-to-print PDF file.',

      'page callback'     => 'theme',
      'page arguments'    => array(QRSBR_THEME_RENDER_PAGE),

      'access arguments'  => array(QRSBR_PERM_VIEW_ALL),
    ),

    QRSBR_PATH_PROCESS => array(
      'type'              => MENU_CALLBACK,
      'title'             => 'QR-code sticker sheet bulk render callback',
      'description'       => 'The callback for initiating the QR-code sticker sheet bulk rendering process.',

      'page callback'     => 'qrs_sheet_bulk_render_process',

      'access arguments'  => array(QRSBR_PERM_VIEW_ALL),
    ),
  );

  return $items;
}

/**
 * Implementation of hook_perm().
 *
 * @return
 *    A list of the permissions defined by this module.
 */
function qrs_sheet_bulk_render_perm() {
  $permissions = array(
    QRSBR_PERM_VIEW_ALL
  );

  return $permissions;
}

/**
 * Implementation of hook_theme().
 *
 * @return A keyed array of theme hooks for this module.
 */
function qrs_sheet_bulk_render_theme() {
  return array(
    QRSBR_THEME_RENDER_PAGE => array(
      'arguments' => array(),
      'file'      => QRSBR_MODULE . '.theme.inc',
    ),
  );
}

/**
 * A menu callback that initializes the batch operation for rendering all QR-code sticker sheet content into a single
 * PDF.
 */
function qrs_sheet_bulk_render_process() {
  // ** sigh ** There doesn't seem to be a Drupal way to do this.
  $result   = db_query("SELECT nid FROM {node} WHERE type = '" . QRS_CONTENT_TYPE . "' GROUP BY nid ORDER BY nid");
  $nodeIds  = array();

  while ($nodeId = db_fetch_object($result)) {
    $nodeIds[] = $nodeId->nid;
  }

  if (!empty($nodeIds)) {
    $renderBatch  = array(
      'finished'      => 'qrs_sheet_bulk_render_batch_finished',
      'title'         => t('Rendering sticker sheet(s) to PDF'),
      'init_message'  => t('Sticker sheet bulk rendering process is starting.'),
      'error_message' => t('Sticker sheet bulk rendering process encountered an error.'),

      'operations'    => array(array('qrs_sheet_bulk_render_batch', array($nodeIds))),
    );

    batch_set($renderBatch);

    // In case of an error, send the user back to the first page.
    batch_process(QRSBR_PATH_PAGE);
  }
  else {
    // Nothing to render...
    drupal_set_message(t('There are no sticker sheets to render at this time.'), 'error');
    drupal_goto(QRSBR_PATH_PAGE);
  }
}

/**
 * Batch API operation callback for rendering QR-code sticker sheet nodes to the PDF of all sticker sheets.
 *
 * @param $nodeIds
 *    An array of the node identifiers for the nodes that are to be rendered to the PDF.
 *
 * @param $context
 *    The reference to the array of context information from the Batch API, including the overall operation progress.
 */
function qrs_sheet_bulk_render_batch(array $nodeIds, &$context) {
  $indexKey     = 'sheetIndex';

  if (empty($context['sandbox'])) {
    _qrs_sheet_bulk_render_batch_initialize_context($context);

    $tempFileName = $context['results'];

    // Start out with an empty PDF; rendering the first sticker sheet will initialize the object.
    $pdf  = NULL;
  }
  else {
    $tempFileName = $context['results'];

    $pdf  = _qrs_sheet_bulk_render_load_from_temp_file($tempFileName);

    if (!pdf)
      die();  // An error will be returned to the user
  }

  $sheetIndex       = &$context['sandbox'][QRSBR_SHEET_INDEX_KEY];
  $totalSheetCount  = count($nodeIds);

  // Render several sheets per request to improve performance by reducing serialization & file IO overhead
  $numberToProcessThisRequest = min(QRSBR_MAX_SHEETS_PER_REQUEST, ($totalSheetCount - $sheetIndex));

  for ($i = 0; $i < $numberToProcessThisRequest; ++$i) {
    $nodeId       = $nodeIds[$sheetIndex];
    $node         = node_load($nodeId, NULL, TRUE);

    _qrs_sheets_prepare_content($node);

    if (empty($node)) {
      drupal_set_message(
        t('QR-sticker sheet node #!nid unexpectedly failed to load.', array('%nid' => $nodeId)),
        'error');

      die();
    }
    else {
      _qrs_sheets_render_sheet($node, $pdf);

      ++$sheetIndex;

      _qrs_sheet_bulk_render_batch_update_status($context, $sheetIndex, $totalSheetCount);
    }
  }

  if (!_qrs_sheet_bulk_render_save_to_temp_file($tempFileName, $pdf))
    die();  // An error will be returned to the user
}

/**
 * Batch API callback for when the QR-code sticker sheet rendering process has completed, either successfully
 * (<code>$success</code> is <code>TRUE</code>), or unsuccessfully (<code>$success</code> is <code>FALSE</code>.
 *
 * If successful, <code>$results</code> should contain the filename for the file that temporarily contains the
 * serialized PDF data that is ready to be sent back to the client as a PDF document.
 *
 * @param $success
 *    Whether or not the node generation operation completed successfully.
 *
 * @param $results
 *    An array of the result data provided by the batch operation callback.
 *
 * @param $operations
 *    An array of the operations that were performed, as callback function names and argument arrays.
 */
function qrs_sheet_bulk_render_batch_finished($success, $results, $operations) {
  if ($success) {
    $tempFileName = $results;
    $pdf          = _qrs_sheet_bulk_render_load_from_temp_file($tempFileName);

    // The temp file is no longer needed, so delete it.
    file_delete($results);

    if (!empty($pdf)) {
      // Make the PDF.
      $pdf->Output("All sticker sheets.pdf", 'I');

      exit();
    }
    else {
      // Display an error
      $success = FALSE;
    }
  }

  // Check again -- could have failed before final PDF rendering
  if (!$success) {
    drupal_set_message(
      t('One or more errors were encountered while rendering sticker sheets to PDF. Please check the site log.'),
      'error');
  }
}

/**
 * Utility function for initializing the Batch API context array.
 *
 * @param $context
 *    The reference to the array of context information from the Batch API, including the overall operation progress.
 */
function _qrs_sheet_bulk_render_batch_initialize_context(&$context) {
  $context['sandbox'][QRSBR_SHEET_INDEX_KEY]  = 0;

  // Create a temporary scratch file
  $tempFileName = _qrs_sheet_bulk_render_create_temp_file();

  if ($tempFileName == FALSE) {
    drupal_set_message(t('Unable to create a temporary file for PDF rendering.'), 'error');
  }

  // Save the temp file name as the results.
  $context['results'] = $tempFileName;
}

/**
 * Update the status information used by the Batch API, including total progress and status message.
 *
 * @param $sheetIndex
 *    The current sheet index in the array of node identifiers.
 *
 * @param $totalSheetCount
 *    The total number of sheets to render to PDF.
 */
function _qrs_sheet_bulk_render_batch_update_status(&$context, $sheetIndex, $totalSheetCount) {
  $context['message']  =
    t('Rendered @current out of @total sticker sheets to PDF.',
      array(
        '@current'  => $sheetIndex,
        '@total'    => $totalSheetCount));

  $context['finished']  = ($sheetIndex / $totalSheetCount);
}

/**
 * Utility function for creating a temporary file and returning its name.
 *
 * TODO: Find out if Drupal already has a way to do this.
 *
 * @return  The name of the new temporary file, or <code>FALSE</code> if a temporary file cannot be created.
 */
function _qrs_sheet_bulk_render_create_temp_file() {
  $tempDirectory  = realpath(file_directory_temp());
  $tempFilename   = tempnam($tempDirectory, QRSBR_MODULE);

  if (touch($tempFilename))
    $result = $tempFilename;

  else
    $result = FALSE;

  return $result;
}

/**
 * Utility function for loading the current PDF data from the specified temporary file and returning it as an
 * FPDF instance.
 *
 * If data cannot be loaded from the specified temporary file, the appropriate Drupal error message(s) will be set.
 *
 * @param $tempFile
 *    The name of the temporary file from which to load.
 *
 * @return
 *    An FPDF instance if the data was loaded successfully from the specified temporary file, or <code>NULL</code>
 *    if the data could not be loaded.
 */
function _qrs_sheet_bulk_render_load_from_temp_file($tempFile) {
  $tempData = @file_get_contents($tempFile);

  if (empty($tempData)) {
    drupal_set_message(
      t('The temporary file for PDF rendering (@file) has been deleted or is unexpectedly empty.',
        array('@file' => $tempFile)),
      'error');

    $pdf  = NULL;
  }
  else {
    $pdf  = unserialize($tempData);

    if (empty($pdf) || (!($pdf instanceof FPDF))) {
      drupal_set_message(
        t('The data in the temporary file for PDF rendering (@file) was not an FPDF object.'),
          array('@file' => $tempFile),
        'error');

      $pdf  = NULL;
    }
  }

  return $pdf;
}

/**
 * Utility function for saving the specified PDF data to the specified temporary file.
 *
 * If data cannot be saved to the specified temporary file, the appropriate Drupal error message(s) will be set.
 *
 * @param $tempFile
 *    The name of the temporary file to which to save.
 *
 * @param $pdf
 *    The FPDF instance to serialize and save to the temporary file.
 *
 * @return
 *    <code>TRUE</code> if the data was successfully written to the specified temporary file, <code>FALSE</code>
 *    otherwise.
 */
function _qrs_sheet_bulk_render_save_to_temp_file($tempFile, FPDF $pdf) {
  $tempData = serialize($pdf);

  $result = @file_put_contents($tempFile, $tempData);

  if (!$result) {
    drupal_set_message(
      t('Temporary data could not be written to the temporary file for PDF rendering (@file).',
        array('@file' => $tempFile),
      'error'));
  }

  return $result;
}
