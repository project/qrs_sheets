<?php

/**
 * @file
 * Constants used by the "QR-Code Sticker Sheet Generator" module.
 *
 * @author
 * Guy Paddock (gapopl@rit.edu)
 */

/**
 * Identifier for the form that allows users to generate QR-code PDF sticker sheet nodes in bulk.
 *
 * @var string
 */
define('QRSG_FORM', 'qrs_sheet_generator_form');

/**
 * The name of the field in which the user enters the number of stickers he or she would like to generate.
 *
 * @var string
 */
define('QRSG_FIELD_DESIRED_SHEET_COUNT', 'desired_sticker_count');

/**
 * The default value of the field specifying how many sheets to generate.
 */
define('QRSG_DEFAULT_SHEET_COUNT', 500);

/**
 * The maximum number of QR-code sticker sheets to generate during a single batch request. The higher this
 * number, the faster the batch process will progress, but the greater the chance Drupal will exceed the PHP
 * script time limit.
 *
 * @var integer
 */
define('QRSG_MAX_SHEETS_PER_REQUEST', 25);

//  All permissions
/**
 * Name of the permission for being able to create QR-code PDF sticker sheet nodes in bulk.
 *
 * @var string
 */
define('QRSG_PERM_CREATE', 'generate QR-code PDF sticker sheets in bulk (also requires permission to create)');