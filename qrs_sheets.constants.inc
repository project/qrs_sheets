<?php

/**
 * @file
 * Constants used by the "QR-Code PDF Sticker Sheets" module.
 *
 * @author
 * Guy Paddock (gapopl@rit.edu)
 */

/**
 * The internal name for the QR-code PDF sticker sheet content type.
 *
 * @var string
 */
define('QRS_CONTENT_TYPE', 'qr_sticker_sheet');

// Field / setting names
/**
 * The name of the field / setting containing the token / pattern for the text in the header of the sticker sheet.
 *
 * @var string
 */
define('QRS_FIELD_HEADER', 'qrs_headerPattern');

/**
 * The name of the field / setting containing the token pattern for the text in the footer of the sticker sheet.
 *
 * @var string
 */
define('QRS_FIELD_FOOTER', 'qrs_footerPattern');

/**
 * The name of the field / setting containing the token pattern for the value embedded in each QR-code on the sticker
 * sheet.
 *
 * @var string
 */
define('QRS_FIELD_CONTENT', 'qrs_contentPattern');

/**
 * The name of the field / setting containing the token pattern for the text in the label below each QR-code on the
 * sticker sheet.
 *
 * @var string
 */
define('QRS_FIELD_LABEL', 'qrs_labelPattern');

/**
 * The name of the "packed" setting / setting fieldset that contains all of the settings for the first QR-code sticker
 * of a sticker sheet.
 *
 * @var string
 */
define('QRS_FIELD_FIRST_STICKER', 'qrs_firstSticker');

/**
 * The name of the field / setting that specifies whether the first sticker of a QR-code sticker sheet should have
 * a different layout than the other stickers.
 *
 * @var string
 */
define('QRS_FIELD_FIRST_DIFFERENT', 'qrs_isDifferent');

//  All permissions
/**
 * Name of the permission for being able to create a QR-code PDF sticker sheet node.
 *
 * @var string
 */
define('QRS_PERM_CREATE', 'create QR-code PDF sticker sheet content');

/**
 * Name of the permission for being able to delete one's own QR-code PDF sticker sheet node.
 *
 * @var string
 */
define('QRS_PERM_DELETE_OWN', 'delete own QR-code PDF sticker sheet content');

/**
 * Name of the permission for being able to delete any QR-code PDF sticker sheet node.
 *
 * @var string
 */
define('QRS_PERM_DELETE_ANY', 'delete any QR-code PDF sticker sheet content');

/**
 * Name of the permission for being able to edit one's own QR-code PDF sticker sheet node.
 *
 * @var string
 */
define('QRS_PERM_EDIT_OWN', 'edit own QR-code PDF sticker sheet content');

/**
 * Name of the permission for being able to edit any QR-code PDF sticker sheet node.
 *
 * @var string
 */
define('QRS_PERM_EDIT_ANY', 'edit any QR-code PDF sticker sheet content');